package com.example.yrchallenge.ui.forecast

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.adapters.ForecastListAdapter
import com.example.yrchallenge.R
import com.example.yrchallenge.base.YrFragment
import kotlinx.android.synthetic.main.forecast_fragment.*

/**
 * Fragment for displaying a forecast.
 * Creates a column of cards with forecast for each day
 */
class ForecastFragment : YrFragment() {
    private val args: ForecastFragmentArgs by navArgs()

    private val forecastsAdapter: ForecastListAdapter by lazy { ForecastListAdapter() }

    private lateinit var presenter: ForecastFragmentPresenter
    private lateinit var viewModel: ForecastViewModel

    private fun updateLoadingState(state: DataLoadState) {
        when (state.state) {
            DataLoadState.State.FINISHED -> showResultsView()
            DataLoadState.State.LOADING -> showProgressBar()
            DataLoadState.State.ERROR -> showError(state.message)
        }
    }

    private fun showResultsView() {
        progressForecasts.visibility = View.GONE
        recyclerForecasts.visibility = View.VISIBLE
    }

    private fun showProgressBar() {
        recyclerForecasts.visibility = View.GONE
        progressForecasts.visibility = View.VISIBLE
    }

    private fun showError(err: String?) {
        showResultsView()

        val text = err ?: getString(R.string.error_general)
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    private fun setupPresenter() {
        presenter = ForecastFragmentPresenter(this, yrApi, args.locationId)
    }

    private fun setupRecyclerView() {
        recyclerForecasts.apply {
            adapter = forecastsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(ForecastViewModel::class.java).apply {
            forecastsModel.observe(this@ForecastFragment, Observer {
                forecastsAdapter.items = it
            })
            loadStateModel.observe(this@ForecastFragment, Observer {
                updateLoadingState(it)
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.forecast_fragment, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
        setupViewModel()
        setupPresenter()
    }
}
