package com.example.yrchallenge.ui.forecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.data.Forecast
import com.example.yrchallenge.data.SingleDayForecast
import com.example.yrchallenge.runOnMain


class ForecastViewModel : ViewModel() {
    private val forecastDataModel: MutableLiveData<List<SingleDayForecast>> = MutableLiveData()
    private val stateModel: MutableLiveData<DataLoadState> = MutableLiveData()

    val forecastsModel: LiveData<List<SingleDayForecast>>
        get() = forecastDataModel
    val loadStateModel: LiveData<DataLoadState>
        get() = stateModel

    // Group list by forecast start date
    fun setForecasts(forecasts: List<Forecast>) {
        val singleDayForecasts: List<SingleDayForecast> = forecasts.groupBy(
            { it.dateDisplayString }, { it }
        ).entries.map { entry ->
            SingleDayForecast(
                displayDate = entry.key,
                forecasts = entry.value
            )
        }

        updateForecasts(singleDayForecasts)
    }

    fun setLoadState(state: DataLoadState) {
        runOnMain {
            stateModel.value = state
        }
    }

    private fun updateForecasts(list: List<SingleDayForecast>) {
        runOnMain {
            forecastDataModel.value = list
        }
    }
}
