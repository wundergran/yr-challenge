package com.example.yrchallenge.data

import org.junit.Test
import org.junit.Assert.assertEquals
import java.time.Duration
import java.util.*

class ForecastTest {

    @Test
    fun `Forecast date duration test`() {
        val startDate = Date(Duration.ofMinutes(15).toMillis())
        val endDate = Date(Duration.ofHours(1)
            .plus(Duration.ofMinutes(40))
            .toMillis()
        )
        val forecast = Forecast(start = startDate, end = endDate)
        val expectedText = "01 - 02"
        val actualText = forecast.durationText

        assertEquals(expectedText, actualText)
    }

    @Test
    fun `ForecastTemperature displayValue test`() {
        val temp = ForecastTemperature(6.2)
        val expectedDisplayText = "6°C"
        val actualDisplayText = temp.displayValue
        assertEquals(expectedDisplayText, actualDisplayText)
    }

    @Test
    fun `ForecastPrecipitation displayValue test`() {
        val precipitation = ForecastPrecipitation(0.1, 1.9, 0.7)
        val expectedDisplayText = "0.1 - 1.9mm"
        val actualDisplayText = precipitation.displayValue
        assertEquals(actualDisplayText, expectedDisplayText)
    }

    @Test
    fun `create forecast verify dateDisplayString`() {
        val forecast = Forecast(
            start = Date(1574958418653), // 28.11.2019 17:25
            end = Date(1574964744691) // 28.11.2019 19:13
        )

        val expectedText = "Thursday 28 November"
        val actualText = forecast.dateDisplayString

        assertEquals(expectedText, actualText)
    }
}