package com.example.yrchallenge

/**
 * Simple class containing a loading state and an optional message.
 */
data class DataLoadState private constructor(
    val state: State,
    val message: String? = null
) {

    companion object {
        fun finished() = DataLoadState(State.FINISHED)
        fun loading() = DataLoadState(State.LOADING)
        fun error(msg: String?) = DataLoadState(State.ERROR, msg)
    }

    enum class State {
        FINISHED, LOADING, ERROR
    }
}