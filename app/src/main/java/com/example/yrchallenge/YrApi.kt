package com.example.yrchallenge

import com.example.yrchallenge.data.ForecastResult
import com.example.yrchallenge.data.LocationSearchResult
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

// http for invalid ssl cert
private const val BASE_API_URL = "http://yr.no/api/v0/"

/**
 * An interface for communicating with backend.
 *
 * @see [createApi]
 */
interface YrApi {
    @GET("locations/Search")
    fun locationsSearch(@Query("q") location: String): Call<LocationSearchResult>

    @GET("locations/{id}/forecast")
    fun locationForecast(@Path("id") id: String): Call<ForecastResult>
}

/**
 * Function for creating and configuring a retrofit instance
 *
 * @return instance of [YrApi] for calling backend
 */
fun createApi() : YrApi {
    val moshi = createMoshi()
    val retrofit = Retrofit.Builder().apply {
        baseUrl(BASE_API_URL)
        addConverterFactory(MoshiConverterFactory.create(moshi))
    }.build()

    return retrofit.create(YrApi::class.java)
}

/**
 * Function for creating and configuring a moshi instance
 *
 * @return instance of [Moshi] configured for serializing JSON
 */
private fun createMoshi() : Moshi = Moshi.Builder().apply {
    add(KotlinJsonAdapterFactory())
    add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
}.build()