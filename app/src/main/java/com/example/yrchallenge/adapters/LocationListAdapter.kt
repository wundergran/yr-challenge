package com.example.yrchallenge.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.yrchallenge.R
import com.example.yrchallenge.data.Location
import kotlinx.android.synthetic.main.location_row.view.*

/**
 * RecyclerView adapter for displaying location search results
 */
class LocationListAdapter : RecyclerView.Adapter<LocationViewHolder>() {

    var items: List<Location> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onItemClick: (location: Location) -> Unit = { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.location_row, parent, false)

        return LocationViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val location: Location = items[position]
        holder.itemView.apply {
            text_title.text = location.name
            text_subtitle.text = location.category?.name ?: ""
            setOnClickListener {
                onItemClick(location)
            }
        }
    }

}

class LocationViewHolder(view: View) : RecyclerView.ViewHolder(view)