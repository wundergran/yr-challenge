package com.example.yrchallenge

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    private val navController: NavController
        get() = findNavController(R.id.nav_host_fragment)

    /**
     * Bind NavController to the activity ActionBar
     * so it can set label according to fragment shown.
     */
    private fun setupNavigation(controller: NavController) {
        setupActionBarWithNavController(controller)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setupNavigation(navController)
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()
}
