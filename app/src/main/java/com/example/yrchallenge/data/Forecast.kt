package com.example.yrchallenge.data

import android.content.Context
import com.squareup.moshi.Json
import java.util.*
import kotlin.math.roundToInt


/**
 * Class containing a single forecast for a specified duration
 */
data class Forecast(
    val symbol: ForecastSymbol? = null,
    val precipitation: ForecastPrecipitation? = null,
    val temperature: ForecastTemperature? = null,
    val wind: ForecastWind? = null,
    val pressure: ForecastPressure? = null,
    val cloudCover: ForecastCloudCover? = null,
    val humidity: ForecastHumidity? = null,
    val dewPoint: ForecastDewPoint? = null,
    val start: Date,
    val end: Date
) {
    val durationText: String by lazy { makeDurationText() }
    val dateDisplayString: String by lazy { makeDateDisplayString(start) }

    private fun makeDurationText(): String {
        // Make sure int converts to double digit string
        val conversion: (Int) -> String = {
            if (it < 10) "0$it"
                else "$it"
        }

        val startHourString: String = Calendar.getInstance().apply { time = start }
            .get(Calendar.HOUR_OF_DAY).let { conversion(it) }

        val endHourString: String = Calendar.getInstance().apply { time = end }
            .get(Calendar.HOUR_OF_DAY).let { conversion(it) }

        return "$startHourString - $endHourString"
    }

    /**
     * Creates a date string for displaying.
     * For example Thursday 28 November
     */
    private fun makeDateDisplayString(date: Date): String {
        val calendar = Calendar.getInstance().apply { time = date }

        return "${
        calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH)
        } ${
        calendar.get(Calendar.DATE)
        } ${
        calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH)
        }"
    }
}

data class ForecastSymbol(
    val n: Int? = null,
    val clouds: Int? = null,
    val precipPhase: String? = null,
    val precip: Int? = null,
    val sunup: Boolean? = null,

    @Json(name = "var")
    val variant: String? = null
) {
    /**
     * Parses this ForecastSymbol's n value into a resource identifier
     *
     * @return id for weather drawable or 0 if not existing
     */
    fun getWeatherIconIdentifier(context: Context): Int {
        val id = when (variant) {
            "Moon" -> "${n}n"
            "Sun" -> "${n}d"
            else -> n?.toString()
        } ?: return 0

        return context.resources.getIdentifier(
            "weather_$id",
            "drawable",
            context.packageName
        )
    }
}

data class ForecastPrecipitation(
    val min: Double? = null,
    val max: Double? = null,
    val value: Double? = null
) {
    val displayValue: String
        get() = "$min - ${max}mm"
}

data class ForecastTemperature(val value: Double) {
    val displayValue = "${value.roundToInt()}°C"
}

data class ForecastWind(
    val direction: Int? = null,
    val gust: Double? = null,
    val speed: Double? = null
)

data class ForecastPressure(
    val value: Int? = null
)

data class ForecastCloudCover(
    val value: Int? = null,
    val high: Int? = null,
    val middle: Int? = null,
    val low: Int? = null,
    val fog: Int? = null
)

data class ForecastHumidity(
    val value: Double
)

data class ForecastDewPoint(
    val value: Double
)

/**
 * Class matching result from api call location/forecast
 */
data class ForecastResult(
    val created: Date,
    val update: Date,
    val shortIntervals: List<Forecast>? = null,
    val longIntervals: List<Forecast>? = null
)

data class SingleDayForecast(
    val displayDate: String,
    val forecasts: List<Forecast>
)