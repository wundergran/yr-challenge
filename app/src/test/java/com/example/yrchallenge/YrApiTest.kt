package com.example.yrchallenge

import com.example.yrchallenge.data.Forecast
import com.example.yrchallenge.data.ForecastResult
import com.example.yrchallenge.data.LocationSearchResult
import kotlinx.coroutines.runBlocking
import org.junit.Test
import retrofit2.await

class YrApiTest {
    private val yrApi: YrApi = createApi()

    @Test
    fun `fetch and verify location search`() = runBlocking {
        val query = "Oslo"
        val result: LocationSearchResult = yrApi.locationsSearch(query).await()

        assert(result.totalResults != null)
        assert(result.getLocations()?.size != null)

        val firstLocation = result.getLocations()?.first()

        assert(firstLocation != null)
        assert(firstLocation?.name != null)
    }

    @Test
    fun `fetch and verify location forecast`() = runBlocking {
        val locationId = "1-143669" // Lillehammer

        val result: ForecastResult = yrApi.locationForecast(locationId).await()

        assert(result.longIntervals != null)
        assert(result.shortIntervals != null)
        assert(result.longIntervals?.isNotEmpty() == true)
        assert(result.shortIntervals?.isNotEmpty() == true)

        val forecastValid: (Forecast) -> Boolean = { it.temperature != null }
        val validShortForecasts: Int = result.shortIntervals?.filter(forecastValid)?.size ?: 0
        val validLongForecasts: Int = result.longIntervals?.filter(forecastValid)?.size ?: 0

        assert(validShortForecasts == result.shortIntervals?.size)
        assert(validLongForecasts == result.longIntervals?.size)
    }
}