package com.example.yrchallenge.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.adapters.LocationListAdapter
import com.example.yrchallenge.R
import com.example.yrchallenge.base.YrFragment
import kotlinx.android.synthetic.main.main_fragment.*

/**
 * Main fragment of the app.
 * Will disply a search input field for location search and display results
 */
class MainFragment : YrFragment() {
    private lateinit var presenter: MainFragmentPresenter
    private lateinit var viewModel: MainFragmentViewModel

    private val recyclerViewAdapter = LocationListAdapter()

    private val locationQuery: String?
        get() = textInputSearch?.text?.toString()

    fun setTextFieldError(errorText: String?) {
        textInputSearch.error = errorText
    }


    private fun setupPresenter() {
        presenter = MainFragmentPresenter(this, yrApi)
    }

    private fun setupClickListeners() {
        buttonSearch?.setOnClickListener {
            presenter.onSearch(locationQuery)
        }

        textInputSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                presenter.onSearch(locationQuery)
                true
            } else {
                false
            }
        }
    }

    private fun setupRecyclerView() {
        recyclerLocations?.apply {
            adapter = recyclerViewAdapter
            layoutManager = LinearLayoutManager(context)
        }

        recyclerViewAdapter.onItemClick = { location ->
            presenter.onLocationClicked(location)
        }
    }

    /**
     * Sets up [viewModel] and subscribes to state and result changes.
     */
    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this).get(MainFragmentViewModel::class.java).apply {
            searchResult.observe(this@MainFragment, Observer {
                recyclerViewAdapter.items = it
            })

            loadingState.observe(this@MainFragment, Observer {
                onLoadingChange(it)
            })
        }
    }

    private fun onLoadingChange(state: DataLoadState) {
        when (state.state) {
            DataLoadState.State.LOADING -> showProgressSpinner()
            DataLoadState.State.FINISHED -> showResultsView()
            DataLoadState.State.ERROR -> showError(state.message)
        }
    }

    private fun showProgressSpinner() {
        recyclerLocations.visibility = View.GONE
        progressLocations.visibility = View.VISIBLE
    }

    private fun showResultsView() {
        recyclerLocations.visibility = View.VISIBLE
        progressLocations.visibility = View.GONE
    }

    private fun showError(msg: String?) {
        showResultsView()
        val text = msg ?: getString(R.string.error_general)

        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.main_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupClickListeners()
        setupRecyclerView()
        setupPresenter()
    }
}
