package com.example.yrchallenge

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Shorthand convenience function for running a task on the main thread using Coroutines.
 *
 * @return Job generated by [GlobalScope.launch]
 */
fun runOnMain(task: CoroutineScope.() -> Unit) = GlobalScope.launch(Dispatchers.Main) { task() }