package com.example.yrchallenge.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.data.Location
import com.example.yrchallenge.runOnMain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainFragmentViewModel : ViewModel() {
    private val mutableSearchResults: MutableLiveData<List<Location>> = MutableLiveData()
    private val mutableLoadingState: MutableLiveData<DataLoadState> = MutableLiveData()

    val searchResult: LiveData<List<Location>>
        get() = mutableSearchResults
    val loadingState: LiveData<DataLoadState>
        get() = mutableLoadingState

    fun setState(state: DataLoadState) {
        runOnMain {
            mutableLoadingState.value = state
        }
    }

    fun setResults(result: List<Location>) {
        runOnMain {
            mutableSearchResults.value = result
        }
    }
}