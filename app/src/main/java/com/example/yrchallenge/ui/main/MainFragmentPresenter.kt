package com.example.yrchallenge.ui.main

import android.util.Log
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.R
import com.example.yrchallenge.YrApi
import com.example.yrchallenge.data.Location
import kotlinx.coroutines.*
import retrofit2.await
import java.lang.Exception

private const val TAG = "MainFragmentPresenter"
class MainFragmentPresenter (
    private val fragment: MainFragment,
    private val api: YrApi) {

    private val viewModel: MainFragmentViewModel = ViewModelProviders.of(fragment).get(MainFragmentViewModel::class.java)

    /**
     * Function called by view when user wants to search.
     * Will perform a search if query is valid and api is available
     */
    fun onSearch(query: String?) {
        if (query.isNullOrBlank()) {
            fragment.setTextFieldError(fragment.getString(R.string.error_search_blank))
        } else {
            searchAndDisplay(query, api)
        }
    }

    /**
     * Function called by view when user clicked a search result from list.
     * Will navigate to fragment for fetching forecast if location is valid.
     */
    fun onLocationClicked(location: Location) {
        Log.d(TAG, "Location clicked: $location")
        location.id?.also { id ->
            navigateToLocationOverview(id, location.name)
        }
    }

    /**
     * Function sets loading state to [viewModel] and performs API request.
     * will call [onSearchSuccess] if result of request is valid
     * or [onSearchError] if it has an error message
     */
    private fun searchAndDisplay(query: String, api: YrApi) {
        viewModel.setState(DataLoadState.loading())
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val searchResult: List<Location>? = api
                    .locationsSearch(query).await()
                    .getLocations()

                if (searchResult?.isNotEmpty() == true) {
                    onSearchSuccess(searchResult)
                } else {
                    onSearchError(fragment.getString(R.string.error_search_no_results))
                }
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
                onSearchError(e.localizedMessage)
            }
        }
    }

    private fun navigateToLocationOverview(locationId: String, locationName: String?) {
        val action = MainFragmentDirections.actionMainFragmentToForecastFragment(
            locationName = locationName,
            locationId = locationId
        )

        fragment.findNavController().navigate(action)
    }

    /**
     * Send error state to [viewModel]
     */
    private fun onSearchError(errorMsg: String?) {
        val msg = errorMsg ?: fragment.getString(R.string.error_general)
        viewModel.setState(DataLoadState.error(msg))
    }

    /**
     * Sends [DataLoadState.State.FINISHED] and results to [viewModel]
     */
    private fun onSearchSuccess(result: List<Location>) {
        Log.d(TAG, "Search success, ${result.size} items set")
        viewModel.setState(DataLoadState.finished())
        viewModel.setResults(result)
    }
}