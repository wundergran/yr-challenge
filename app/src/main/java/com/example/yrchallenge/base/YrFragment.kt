package com.example.yrchallenge.base

import androidx.fragment.app.Fragment
import com.example.yrchallenge.YrApi
import com.example.yrchallenge.createApi

/**
 * Base fragment holding a reference to the API
 */
abstract class YrFragment : Fragment() {
    val yrApi: YrApi by lazy { createApi() }
}