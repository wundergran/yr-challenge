package com.example.yrchallenge.data

/**
 * Simple data class for describing a location from API
 */
data class Location(
    val id: String?,
    val name: String?,
    val position: Position?,
    val category: LocationCategory?,
    val elevation: Int?
)

data class Position(
    val lat: Double,
    val lon: Double
)

data class LocationCategory(
    val id: String,
    val name: String?
)

/**
 *  Data class describing a search result response from API
 */
data class LocationSearchResult(
    val totalResults: Int?,
    val _embedded: LocationSearchEmbedded?
) {
    fun getLocations(): List<Location>? = _embedded?.location
}

data class LocationSearchEmbedded(
    val location: List<Location>?
)