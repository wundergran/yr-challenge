package com.example.yrchallenge.ui.forecast

import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.example.yrchallenge.DataLoadState
import com.example.yrchallenge.R
import com.example.yrchallenge.YrApi
import com.example.yrchallenge.data.Forecast
import kotlinx.coroutines.*
import retrofit2.await
import java.lang.Exception

private const val TAG = "ForecastFragmentPresenter"
class ForecastFragmentPresenter(
    private val fragment: ForecastFragment,
    private val api: YrApi,
    private val locationId: String
) {
    private val viewModel = ViewModelProviders.of(fragment).get(ForecastViewModel::class.java)

    init {
        getData()
    }

    /**
     * Function for fetching a forecast for [locationId]
     * Will call either [onFetchSuccess] with result or [onFetchError] when complete
     */
    private fun getData() {
        viewModel.setLoadState(DataLoadState.loading())
        GlobalScope.launch(Dispatchers.IO) {

            val shortIntervalForecasts = fetchAsync(locationId).await()
            if (shortIntervalForecasts != null) {
                onFetchSuccess(shortIntervalForecasts)
            } else {
                onFetchError(fragment.getString(R.string.error_forecast_fetch_failed))
            }
        }
    }

    private fun fetchAsync(locationId: String): Deferred<List<Forecast>?> = GlobalScope.async {
        try {
            val result = api.locationForecast(locationId).await()
            return@async result.shortIntervals
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            onFetchError(e.localizedMessage)
        }

        return@async null
    }

    private fun onFetchSuccess(result: List<Forecast>) {
        viewModel.setForecasts(result)
        viewModel.setLoadState(DataLoadState.finished())
    }

    private fun onFetchError(message: String?) {
        viewModel.setLoadState(DataLoadState.error(message))
    }
}