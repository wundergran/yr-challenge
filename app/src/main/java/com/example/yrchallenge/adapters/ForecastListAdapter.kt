package com.example.yrchallenge.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.yrchallenge.R
import com.example.yrchallenge.data.Forecast
import com.example.yrchallenge.data.SingleDayForecast
import kotlinx.android.synthetic.main.forecast_card.view.*
import kotlinx.android.synthetic.main.forecast_table_row.view.*
import kotlin.math.roundToInt

/**
 * Adapter for displaying a list of [Forecast] in a [RecyclerView]
 */
class ForecastListAdapter : RecyclerView.Adapter<ForecastViewHolder>() {
    var items: List<SingleDayForecast> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.forecast_card, parent, false)

        return ForecastViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        val forecast = items[position]
        holder.setData(forecast)
    }

}

class ForecastViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun setData(singleDayForecast: SingleDayForecast) = itemView.apply {
        text_card_title.text = singleDayForecast.displayDate

        val inflater = LayoutInflater.from(context)
        val forecastRows: List<TableRow> = singleDayForecast.forecasts.map { forecast ->
            val row = inflater.inflate(
                R.layout.forecast_table_row,
                table_forecasts,
                false
            ) as TableRow

            return@map row.apply { setForecastRowData(forecast) }
        }

        table_forecasts.removeAllViews()
        forecastRows.forEach {
            table_forecasts.addView(it)
        }
    }

    private fun TableRow.setForecastRowData(forecast: Forecast) {
        text_time.text = forecast.durationText
        text_degrees.text = forecast.temperature?.displayValue
        text_percipation.text = forecast.precipitation?.displayValue
        text_wind_speed.text = forecast.wind?.speed?.roundToInt()?.toString()
        image_wind_direction.rotation = forecast.wind?.direction?.toFloat() ?: 0f

        forecast.symbol?.also {
            Glide.with(this)
                .load(it.getWeatherIconIdentifier(context))
                .into(image_symbol)
        }
    }
}